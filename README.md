# EXITtrellis
---

Optimizes sequences of LDPC decoding rules on a per-iteration basis.

### Dependencies

- Boost library

- GSL library

- DElib: bitbucket.org:francoislp/delib.git

- config: This library is included as a submodule and is hosted on
  github. If the project has been cloned without it (by executing git
  clone without --recursive), the submodules can be retrieved in a
  second step by executing the following command from the project's
  base directory:

```sh
> git submodule update --init
```

### Build Instructions

The software can be built using cmake. First create a `build` directory.

From the project top-level directory:
```sh
> mkdir build
> cd build
> cmake ..
```

Finally:
```sh
> make && make install
```
will install the executable in a `run` subdirectory of the main project directory.
