//Author: Francois Leduc-Primeau

#ifndef EnergyCost_hpp_
#define EnergyCost_hpp_

#include "ICost.hpp"

class EnergyCost : public ICost {
public:

  EnergyCost() : m_energy(0) {}

  EnergyCost(double energy) : m_energy(energy) {}

  /**
   * Calls the (implicit) copy constructor of the derived class and
   * returns a base class pointer.
   */
  ICost* getCopy() const { return new EnergyCost(*this); }

  double energy() const { return m_energy; }

  ///@see ICost::latency()
  uint latency() const { return -1; }

  double oneDRepresentation() const { return energy(); }

  /**
   * Adds another cost c2 to this one.
   *@throws std::bad_cast If c2 is not of type EnergyCost.
   */
  void operator+=(ICost const& c2) {
    EnergyCost const& ec2 = dynamic_cast<EnergyCost const&>(c2);
    m_energy+= ec2.energy();
  }

  //TODO: currently not implemented
  void addPenalty(double newPenalty) { throw std::exception(); }  

  /// Always returns true and does not throw any exception.
  bool isComparable(ICost const& c2) const { return true; }

  ///@throws std::bad_cast If c2 is not of type EnergyCost.
  bool operator==(ICost const& c2) const {
    EnergyCost const& ec2 = dynamic_cast<EnergyCost const&>(c2);
    return ec2.energy()==m_energy;
  }

  ///@throws std::bad_cast If c2 is not of type EnergyCost.
  bool operator!=(ICost const& c2) const {
    EnergyCost const& ec2 = dynamic_cast<EnergyCost const&>(c2);
    return ec2.energy()!=m_energy;
  }

  /**
   * Whether this cost is smaller than c2.
   *@throws std::bad_cast     if c2 is not of type EnergyCost.
   */
  bool operator<(ICost const& c2) const {
    EnergyCost const& ec2 = dynamic_cast<EnergyCost const&>(c2);
    return m_energy < ec2.energy();
  }

  /**
   * Whether this cost is larger than c2.
   *@throws std::bad_cast     if c2 is not of type EnergyCost
   */
  bool operator>(ICost const& c2) const {
    EnergyCost const& ec2 = dynamic_cast<EnergyCost const&>(c2);
    return m_energy > ec2.energy();
  }

  /**
   * Whether this cost is smaller or equal to c2.
   *@throws std::bad_cast     if c2 is not of type EnergyCost
   */
  bool operator<=(ICost const& c2) const {
    EnergyCost const& ec2 = dynamic_cast<EnergyCost const&>(c2);
    return m_energy <= ec2.energy();
  }

  /**
   * Whether this cost is larger or equal to c2.
   *@throws std::bad_cast     if c2 is not of type EnergyCost
   */
  bool operator>=(ICost const& c2) const {
    EnergyCost const& ec2 = dynamic_cast<EnergyCost const&>(c2);
    return m_energy >= ec2.energy();
  }

  std::ostream& toStream(std::ostream& stream) const {
    return stream << m_energy;
  }

private:

  double m_energy;
};

#endif
