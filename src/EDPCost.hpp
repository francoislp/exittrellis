//Author: Francois Leduc-Primeau

#ifndef EDPCost_hpp_
#define EDPCost_hpp_

#include "ICost.hpp"
#include "types.hpp"
#include <stdexcept>

/**
 * Cost defined as the product of energy and latency (i.e. the product
 * of a positive real number with a positive integer).
 */
class EDPCost : public ICost {
public:
  EDPCost()
    : m_energy(0.0),
      m_latency(0),
      m_penalty(0.0)
  {}

  EDPCost(double energy, uint latency)
    : m_energy(energy),
      m_latency(latency),
      m_penalty(0.0)
  {}

  EDPCost(double energy, uint latency, double penalty)
    : m_energy(energy),
      m_latency(latency),
      m_penalty(penalty)
  {}

  /**
   * Calls the (implicit) copy constructor of the derived class and
   * returns a base class pointer.
   */
  ICost* getCopy() const { return new EDPCost(*this); }

  double energy() const { return m_energy; }

  uint latency() const { return m_latency; }

  double penalty() const { return m_penalty; }

  double oneDRepresentation() const { return (m_energy+m_penalty)*m_latency; }

  /**
   * Adds another cost c2 to this one.
   *@throws std::bad_cast If c2 is not of type EDPCost.
   */
  void operator+=(ICost const& c2) {
    EDPCost const& edp2 = dynamic_cast<EDPCost const&>(c2);
    m_energy+= edp2.energy();
    m_latency+= edp2.latency();
    m_penalty+= edp2.penalty();
  }

  /**
   * Increases the "penalty" cost, which is factored into the virtual cost.
   */
  void addPenalty(double newPenalty) { m_penalty+= newPenalty; }

  /// Always returns true and does not throw any exception.
  bool isComparable(ICost const& c2) const { return true; }

  ///@throws std::bad_cast If c2 is not of type EDPCost.
  bool operator==(ICost const& c2) const {
    EDPCost const& edp2 = dynamic_cast<EDPCost const&>(c2);
    return oneDRepresentation() == edp2.oneDRepresentation();
  }

  ///@throws std::bad_cast If c2 is not of type EDPCost.
  bool operator!=(ICost const& c2) const {
    EDPCost const& edp2 = dynamic_cast<EDPCost const&>(c2);
    return oneDRepresentation() != edp2.oneDRepresentation();
  }

  /**
   * Whether this cost is smaller than c2.
   *@throws std::bad_cast     if c2 is not of type EDPCost.
   */
  bool operator<(ICost const& c2) const {
    EDPCost const& edp2 = dynamic_cast<EDPCost const&>(c2);
    return *this < edp2;
  }

  bool operator<(EDPCost const& edp2) const {
    return oneDRepresentation() < edp2.oneDRepresentation();
  }

  /**
   * Whether this cost is larger than c2.
   *@throws std::bad_cast     if c2 is not of type EDPCost
   */
  bool operator>(ICost const& c2) const {
    EDPCost const& edp2 = dynamic_cast<EDPCost const&>(c2);
    return *this > edp2;
  }

  bool operator>(EDPCost const& edp2) const {
    return oneDRepresentation() > edp2.oneDRepresentation();
  }

  /**
   * Whether this cost is smaller or equal to c2.
   *@throws std::bad_cast     if c2 is not of type EDPCost
   */
  bool operator<=(ICost const& c2) const {
    EDPCost const& edp2 = dynamic_cast<EDPCost const&>(c2);
    if(*this==edp2) return true;
    return *this < edp2;
  }

  /**
   * Whether this cost is larger or equal to c2.
   *@throws std::bad_cast     if c2 is not of type EDPCost
   */
  bool operator>=(ICost const& c2) const {
    EDPCost const& edp2 = dynamic_cast<EDPCost const&>(c2);
    if(*this==edp2) return true;
    return *this > edp2;
  }

  std::ostream& toStream(std::ostream& stream) const {
    return stream << "("<<m_energy<<", "<<m_latency<<")";
  }

private:
  double m_energy;
  uint   m_latency;
  double m_penalty;

};

#endif
