//Author: Francois Leduc-Primeau

#include "Path.hpp"
#include "State.hpp"
#include <boost/filesystem.hpp>

typedef boost::filesystem::path filepath;

std::ostream& operator<<(std::ostream& stream, const Path& p) {
  std::vector<statep_t> states = p.getStateSeq();
  std::vector<algp_t> algs = p.getAlgSeq();
  stream << "{" << states[0]->getMetric();
  for(uint i=0; i<algs.size(); i++) {
    stream << "-" << algs[i]->getName();
    stream << "-" << states[i+1]->getMetric();
  }
  return stream << "}";
}

void Path::extendDE(algp_t algorithm, IStateManager& sm) {
	statep_t curState = headState();
	m_algSeq.push_back(algorithm);

	if(!m_DE.initialized()) {
		// use the first algorithm in the path to configure the common DE parameters
		config const& c= m_algSeq.at(0)->getConfig();
		m_DE.loadParameters(c);
	}

	// Check if DE has been performed on this path before. If it has
	// not, we need to initialize the DE PMFs.
	if(m_DE.iterationCount()<0) {
		// to reduce opportunities for bugs, we do not allow mixing calls
		// to extend and extendDE
		if(m_algSeq.size() != 1) throw std::runtime_error("extendDE has not been called consistently (cannot mix extend and extendDE)");
		
		m_DE.DEInit(); // this sets iterationCount() to 0
	}
	
	if(!m_DE.initialized() || m_DE.iterationCount()<0) throw std::logic_error("should not happen");	

	// perform one additional DE iteration
	config const& c= m_algSeq[m_algSeq.size()-1]->getConfig();

	// make sure algorithm is compatible with the DE
	if(!m_DE.compatible(c)) throw std::runtime_error("Trying to perform density evolution on a sequence of incompatible algorithms");
			
	// set the deviation for that algorithm
	filepath devFilePath(c.getParamString("EXITtrellis2::btwc_root"));
	devFilePath/= c.getParamString("devpmf_filepath_default");
	m_DE.applyDev(devFilePath.string());

	// perform iteration
	m_DE.iteration();
	// -- end additional DE iteration --
	
	// sanity check
	if(m_DE.iterationCount() != m_algSeq.size()) throw std::logic_error("DE iteration count is invalid");

	// set the next state according to error rate reported by DE
	statep_t nextState= sm.getState(m_DE.currentErrProb());
	m_stateSeq.push_back(nextState);

	// update the cost
	(*m_cost)+= *(algorithm->getCost(curState));
	// if the algorithm is being changed, increase the penalty
	// associated with this path
	if(m_algChangePenalty>0 && m_algSeq.size()>1
	   && algorithm!=m_algSeq[m_algSeq.size()-2]) {
		m_cost->addPenalty(m_algChangePenalty);
	}
}
