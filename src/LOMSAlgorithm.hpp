//Author: Francois Leduc-Primeau

#ifndef LOMSAlgorithm_hpp_
#define LOMSAlgorithm_hpp_

#include "IAlgorithm.hpp"
#include "State.hpp"
#include "types.hpp"
#include <string>
#include <map>
#include <set>

class LOMSAlgorithm : public IAlgorithm {
public:

  /**
   * Construct a new definition of a Layered Offset Min-Sum algorithm
   * based on the provided ExitGen configuration.
   *@param conf      The ExitGen configuration
   *@param costdef   Selects the way the algorithm's cost is reported.
   *@throws sanity_exception if some values in the EXIT result files 
   *                         appear invalid
   */
  LOMSAlgorithm(std::unique_ptr<config> conf, COSTDEF_t costdef);

  std::string getName() const { return m_algName; }

	config const& getConfig() const { return *m_conf; }

  /**
   * Returns the point on the EXIT function corresponding to curState.
   */
  metric_t exitFunct(statep_t curState);

  /**
   * Returns the point on the cost function corresponding to curState.
   *@throws sanity_exception if the interpolated energy cost fails a sanity check.
   */
  std::unique_ptr<cost_t> getCost(statep_t curState);

  bool operator==(IAlgorithm const& a2) const {
    return m_algName == a2.getName();
  }

  bool operator!=(IAlgorithm const& a2) const {
    return m_algName != a2.getName();
  }

private:

  /// *Unique* name of this algorithm.
  std::string m_algName;

	/// ExitGen config for this algorithm
	std::unique_ptr<config> m_conf;

  /**
   * List of points on the EXIT function, either obtained from the
   * Monte-Carlo data or interpolated.
   */
  std::map<metric_t,metric_t> m_exitPoints;

  /**
   * List of points on the energy function, either obtained from circuit
   * simulation or interpolated.
   */
  std::map<metric_t,double> m_energyPoints;

  /**
   * Remember which EXIT points came from simulation data (the others are
   * interpolated).
   */
  std::set<metric_t> m_originalXValsExit;

  /**
   * Remember which energy points came from simulation data (the others are
   * interpolated).
   */
  std::set<metric_t> m_originalXValsEnergy;

  /// Cost class being used to report the algorithm's cost.
  COSTDEF_t m_costdef;

  /// Time required to perform one iteration.
  uint m_latency;

};

#endif
