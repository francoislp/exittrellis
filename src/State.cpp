#include "State.hpp"

using std::shared_ptr;

State::State(stateid_t id, metric_t p)
  : m_ID(id),
    m_p(p)
{}

// bool State::reach(Path& path) {
//   // sanity check: make sure that the path ends at this State
//   if(*(path.headState()) != *this) throw std::exception();

//   // check if there is already a better path that was previously
//   // registered, and also check if this path makes some previously
//   // registered paths irrelevant
//   cost_t const& newCost = path.getCost();
//   auto it = m_pathList.begin();
//   while(it != m_pathList.end()) {
//     if(*(it->headState()) != *this) throw std::exception(); // sanity check
//     cost_t const& curCost = it->getCost();
//     if(newCost.isComparable(curCost)) {
//       if(newCost >= curCost) return false;
//       else it = m_pathList.erase(it); // returns iterator to element after erased one
//     } else { // only increment iterator if we haven't deleted anything
//       ++it;
//     }
//   }
//   // if there is no better path, register it
//   m_pathList.push_back(path);
//   return true;
// }
