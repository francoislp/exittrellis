#include "../types.hpp"
#include "../StateManagerLog.hpp"
#include "../State.hpp"

#include <vector>
#include <memory>
#include <iostream>
#include <algorithm>

using std::vector;
using std::shared_ptr;
using std::cout;
using std::endl;

#define FLOAT_TOLERANCE 0.00000000001

// Sorting function for STL containers containing shared_ptr<State>
struct SortStatePtr {
  bool operator()(shared_ptr<State> s1, shared_ptr<State> s2) {
    return (*s1 < *s2);
  }
};

int main(int argc, char** argv) {

  // Test StateManagerLog::getState(metric_t) against Excel spreadsheet
  // (see "log quant test.xlsx")

  StateManagerLog sm(10, 1); //note: the second parameter is ignored

  // Check if the correct quantized states are created
  vector<metric_t> p_list;
  p_list.push_back(0.0574172432264339);
  p_list.push_back(0.0142606398139501);
  p_list.push_back(0.0835414004867297);
  p_list.push_back(0.045651840806733);
  p_list.push_back(0.0396730185188808);
  p_list.push_back(0.0550451442040279);
  p_list.push_back(0.00806180875334899);
  p_list.push_back(0.015745873461104);
  p_list.push_back(0.0345398313045708);
  p_list.push_back(0.0662075426442546);
  p_list.push_back(0.0517843240806858);
  p_list.push_back(0.0754601113126291);
  p_list.push_back(0.049065978176016);
  p_list.push_back(0.0106083691453695);
  p_list.push_back(0.0403694767936921);
  p_list.push_back(0.0730734427939864);
  p_list.push_back(0.0545860954345267);
  p_list.push_back(0.0159715185336428);
  p_list.push_back(0.0167379731996752);
  p_list.push_back(0.0603239497148004);
  p_list.push_back(0.0239139974049048);
  p_list.push_back(0.0200399050652171);
  p_list.push_back(0.00308244212757981);
  p_list.push_back(0.0459628149851536);
  p_list.push_back(0.0552769616373644);
  p_list.push_back(0.0873885727187205);
  p_list.push_back(1);
  p_list.push_back(0.9);
  p_list.push_back(0.8);
  p_list.push_back(0.75);
  p_list.push_back(0.7);
  p_list.push_back(0.65);
  p_list.push_back(0.6);
  p_list.push_back(0.55);
  p_list.push_back(0.5);
  p_list.push_back(0.45);
  p_list.push_back(0.4);
  p_list.push_back(0.35);
  p_list.push_back(0.3);
  p_list.push_back(0.25);
  p_list.push_back(0.2);
  p_list.push_back(0.15);
  p_list.push_back(0.1);
  p_list.push_back(0.05);

  vector<stateid_t> id_list;
  id_list.push_back(12);
  id_list.push_back(18);
  id_list.push_back(10);
  id_list.push_back(13);
  id_list.push_back(14);
  id_list.push_back(12);
  id_list.push_back(20);
  id_list.push_back(18);
  id_list.push_back(14);
  id_list.push_back(11);
  id_list.push_back(12);
  id_list.push_back(11);
  id_list.push_back(13);
  id_list.push_back(19);
  id_list.push_back(13);
  id_list.push_back(11);
  id_list.push_back(12);
  id_list.push_back(17);
  id_list.push_back(17);
  id_list.push_back(12);
  id_list.push_back(16);
  id_list.push_back(16);
  id_list.push_back(25);
  id_list.push_back(13);
  id_list.push_back(12);
  id_list.push_back(10);
  id_list.push_back(0);
  id_list.push_back(0);
  id_list.push_back(0);
  id_list.push_back(1);
  id_list.push_back(1);
  id_list.push_back(1);
  id_list.push_back(2);
  id_list.push_back(2);
  id_list.push_back(3);
  id_list.push_back(3);
  id_list.push_back(3);
  id_list.push_back(4);
  id_list.push_back(5);
  id_list.push_back(6);
  id_list.push_back(6);
  id_list.push_back(8);
  id_list.push_back(10);
  id_list.push_back(13);

  vector<metric_t> pQuant_list;
  pQuant_list.push_back(0.0630957344480193);
  pQuant_list.push_back(0.0158489319246111);
  pQuant_list.push_back(0.1);
  pQuant_list.push_back(0.0501187233627272);
  pQuant_list.push_back(0.0398107170553497);
  pQuant_list.push_back(0.0630957344480193);
  pQuant_list.push_back(0.01);
  pQuant_list.push_back(0.0158489319246111);
  pQuant_list.push_back(0.0398107170553497);
  pQuant_list.push_back(0.0794328234724281);
  pQuant_list.push_back(0.0630957344480193);
  pQuant_list.push_back(0.0794328234724281);
  pQuant_list.push_back(0.0501187233627272);
  pQuant_list.push_back(0.0125892541179417);
  pQuant_list.push_back(0.0501187233627272);
  pQuant_list.push_back(0.0794328234724281);
  pQuant_list.push_back(0.0630957344480193);
  pQuant_list.push_back(0.0199526231496888);
  pQuant_list.push_back(0.0199526231496888);
  pQuant_list.push_back(0.0630957344480193);
  pQuant_list.push_back(0.0251188643150958);
  pQuant_list.push_back(0.0251188643150958);
  pQuant_list.push_back(0.00316227766016838);
  pQuant_list.push_back(0.0501187233627272);
  pQuant_list.push_back(0.0630957344480193);
  pQuant_list.push_back(0.1);
  pQuant_list.push_back(1);
  pQuant_list.push_back(1);
  pQuant_list.push_back(1);
  pQuant_list.push_back(0.794328234724281);
  pQuant_list.push_back(0.794328234724281);
  pQuant_list.push_back(0.794328234724281);
  pQuant_list.push_back(0.630957344480193);
  pQuant_list.push_back(0.630957344480193);
  pQuant_list.push_back(0.501187233627272);
  pQuant_list.push_back(0.501187233627272);
  pQuant_list.push_back(0.501187233627272);
  pQuant_list.push_back(0.398107170553497);
  pQuant_list.push_back(0.316227766016838);
  pQuant_list.push_back(0.251188643150958);
  pQuant_list.push_back(0.251188643150958);
  pQuant_list.push_back(0.158489319246111);
  pQuant_list.push_back(0.1);
  pQuant_list.push_back(0.0501187233627272);
  
  // keep a vector of all the States
  vector<shared_ptr<State> > stateList;

  for(uint i=0; i<p_list.size(); i++) {
    shared_ptr<State> s = sm.getState(p_list[i]);
    stateList.push_back(s);
    stateid_t curID = s->getID();
    if(curID != id_list[i]) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<<endl;
      return 1;
    }
    metric_t curQuantMetric = s->getMetric();
    metric_t diff = curQuantMetric - pQuant_list[i];
    if(diff > FLOAT_TOLERANCE || diff < -FLOAT_TOLERANCE) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<<endl;
      return 1;
    }
  }

  // Test that States are sorted properly
  sort(stateList.begin(), stateList.end(), SortStatePtr());
  sort(p_list.begin(), p_list.end());
  reverse(p_list.begin(), p_list.end()); // lower probability is "better"
  sort(id_list.begin(), id_list.end());
  sort(pQuant_list.begin(), pQuant_list.end());
  reverse(pQuant_list.begin(), pQuant_list.end());

  // same checks as previously:
  for(uint i=0; i<p_list.size(); i++) {
    shared_ptr<State> s = sm.getState(p_list[i]);
    shared_ptr<State> s2 = stateList[i];
    
    if(s != s2) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<<endl;
      return 1;
    }

    stateid_t curID = s2->getID();
    if(curID != id_list[i]) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<<endl;
      return 1;
    }
    metric_t curQuantMetric = s2->getMetric();
    metric_t diff = curQuantMetric - pQuant_list[i];
    if(diff > FLOAT_TOLERANCE || diff < -FLOAT_TOLERANCE) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<<endl;
      return 1;
    }
  }

  // Double-check the sorting (also tests the ">" operator)
  for(uint i=1; i<stateList.size(); i++) {
    shared_ptr<State> s1 = stateList[i-1];
    shared_ptr<State> s2 = stateList[i];
    if( *s1 > *s2 ) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<< endl;
      return 1;
    }
    if( !(*s1==*s2 || *s1<*s2) ) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<< endl;
      return 1;
    }
  }

  // Other test with 1000 points/decade
  StateManagerLog sm2(1000, 1);

  p_list.clear();
  p_list.push_back(0.072403928140753);
  p_list.push_back(0.0946819170392428);
  p_list.push_back(0.0608751891628323);
  p_list.push_back(0.047200321422766);
  p_list.push_back(0.0546307812261604);
  p_list.push_back(0.0481793489047957);
  p_list.push_back(0.0417895212176395);
  p_list.push_back(0.0852109135946658);
  p_list.push_back(0.00904809755946142);
  p_list.push_back(0.0933599268007097);
  p_list.push_back(0.0875894622768334);
  p_list.push_back(0.0307100982287293);
  p_list.push_back(0.0581338107166165);
  p_list.push_back(0.0494519890178853);
  p_list.push_back(0.0533770977956118);
  p_list.push_back(0.0430705870046603);
  p_list.push_back(0.0856883238218132);
  p_list.push_back(0.0592151194361275);
  p_list.push_back(0.0852213334806254);
  p_list.push_back(0.0295857269929663);
  p_list.push_back(0.0240619867752723);
  p_list.push_back(0.0652118048753733);
  p_list.push_back(0.0526759694437334);
  p_list.push_back(0.0116098426525948);
  p_list.push_back(0.0328127907229263);
  p_list.push_back(0.0687007382083411);
  p_list.push_back(0.0473112943686943);
  p_list.push_back(0.00212617057467522);
  p_list.push_back(0.0603169734308746);
  p_list.push_back(0.00922265979891694);
  p_list.push_back(0.0913550220519076);
  p_list.push_back(0.0125886507163318);
  p_list.push_back(1);
  p_list.push_back(0.9);
  p_list.push_back(0.8);
  p_list.push_back(0.75);
  p_list.push_back(0.7);
  p_list.push_back(0.65);
  p_list.push_back(0.6);
  p_list.push_back(0.55);
  p_list.push_back(0.5);
  p_list.push_back(0.45);
  p_list.push_back(0.4);
  p_list.push_back(0.35);
  p_list.push_back(0.3);
  p_list.push_back(0.25);
  p_list.push_back(0.2);
  p_list.push_back(0.15);
  p_list.push_back(0.0999999999999999);
  p_list.push_back(0.0499999999999999);
  p_list.push_back(0.0004);
  p_list.push_back(0.000001);
  p_list.push_back(0.0000000034);
  p_list.push_back(0.000000000005);
  p_list.push_back(0.009);

  id_list.clear();
  id_list.push_back(1140);
  id_list.push_back(1023);
  id_list.push_back(1215);
  id_list.push_back(1326);
  id_list.push_back(1262);
  id_list.push_back(1317);
  id_list.push_back(1378);
  id_list.push_back(1069);
  id_list.push_back(2043);
  id_list.push_back(1029);
  id_list.push_back(1057);
  id_list.push_back(1512);
  id_list.push_back(1235);
  id_list.push_back(1305);
  id_list.push_back(1272);
  id_list.push_back(1365);
  id_list.push_back(1067);
  id_list.push_back(1227);
  id_list.push_back(1069);
  id_list.push_back(1528);
  id_list.push_back(1618);
  id_list.push_back(1185);
  id_list.push_back(1278);
  id_list.push_back(1935);
  id_list.push_back(1483);
  id_list.push_back(1163);
  id_list.push_back(1325);
  id_list.push_back(2672);
  id_list.push_back(1219);
  id_list.push_back(2035);
  id_list.push_back(1039);
  id_list.push_back(1900);
  id_list.push_back(0);
  id_list.push_back(45);
  id_list.push_back(96);
  id_list.push_back(124);
  id_list.push_back(154);
  id_list.push_back(187);
  id_list.push_back(221);
  id_list.push_back(259);
  id_list.push_back(301);
  id_list.push_back(346);
  id_list.push_back(397);
  id_list.push_back(455);
  id_list.push_back(522);
  id_list.push_back(602);
  id_list.push_back(698);
  id_list.push_back(823);
  id_list.push_back(1000);
  id_list.push_back(1301);
  id_list.push_back(3397);
  id_list.push_back(6000);
  id_list.push_back(8468);
  id_list.push_back(11301);
  id_list.push_back(2045);

  pQuant_list.clear();
  pQuant_list.push_back(0.072443596007499);
  pQuant_list.push_back(0.0948418463300897);
  pQuant_list.push_back(0.0609536897240169);
  pQuant_list.push_back(0.047206304126359);
  pQuant_list.push_back(0.0547015962893971);
  pQuant_list.push_back(0.0481947797625127);
  pQuant_list.push_back(0.0418793565117918);
  pQuant_list.push_back(0.0853100114017589);
  pQuant_list.push_back(0.009057326008982);
  pQuant_list.push_back(0.0935405674147552);
  pQuant_list.push_back(0.0877000821143635);
  pQuant_list.push_back(0.0307609681474071);
  pQuant_list.push_back(0.0582103217770871);
  pQuant_list.push_back(0.049545019080479);
  pQuant_list.push_back(0.0534564359396972);
  pQuant_list.push_back(0.0431519076827765);
  pQuant_list.push_back(0.085703784523037);
  pQuant_list.push_back(0.059292532458);
  pQuant_list.push_back(0.0853100114017589);
  pQuant_list.push_back(0.0296483138952434);
  pQuant_list.push_back(0.0240990542868659);
  pQuant_list.push_back(0.0653130552647472);
  pQuant_list.push_back(0.0527229861422823);
  pQuant_list.push_back(0.0116144861384034);
  pQuant_list.push_back(0.0328851630875983);
  pQuant_list.push_back(0.0687068440014232);
  pQuant_list.push_back(0.047315125896148);
  pQuant_list.push_back(0.00212813904598271);
  pQuant_list.push_back(0.060394862937638);
  pQuant_list.push_back(0.00922571427154762);
  pQuant_list.push_back(0.091411324147025);
  pQuant_list.push_back(0.0125892541179417);
  pQuant_list.push_back(1);
  pQuant_list.push_back(0.901571137605957);
  pQuant_list.push_back(0.801678063387679);
  pQuant_list.push_back(0.751622894018205);
  pQuant_list.push_back(0.701455298419971);
  pQuant_list.push_back(0.650129690343091);
  pQuant_list.push_back(0.601173737483278);
  pQuant_list.push_back(0.550807696405403);
  pQuant_list.push_back(0.500034534976978);
  pQuant_list.push_back(0.45081670454146);
  pQuant_list.push_back(0.400866717627303);
  pQuant_list.push_back(0.350751873952568);
  pQuant_list.push_back(0.300607630262823);
  pQuant_list.push_back(0.250034536169643);
  pQuant_list.push_back(0.200447202736516);
  pQuant_list.push_back(0.150314196609002);
  pQuant_list.push_back(0.1);
  pQuant_list.push_back(0.0500034534976978);
  pQuant_list.push_back(0.000400866717627303);
  pQuant_list.push_back(0.000001);
  pQuant_list.push_back(3.40408189701E-09);
  pQuant_list.push_back(5.00034534976977E-12);
  pQuant_list.push_back(0.00901571137605957);

  for(uint i=0; i<p_list.size(); i++) {
    shared_ptr<State> s = sm2.getState(p_list[i]);
    stateid_t curID = s->getID();
    if(curID != id_list[i]) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<<endl;
      return 1;
    }
    metric_t curQuantMetric = s->getMetric();
    metric_t diff = curQuantMetric - pQuant_list[i];
    if(diff > FLOAT_TOLERANCE || diff < -FLOAT_TOLERANCE) {
      cout << "Test fails at line " << __LINE__ << " with i="<<i<<endl;
      return 1;
    }
  }

  cout << "(PASS)" << endl;
  return 0;
}
