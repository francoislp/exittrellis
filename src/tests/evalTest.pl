#!/opt/local/bin/perl -w

use strict;

use Math::BigFloat;
use Math::BigRat;

my $nextp = GallagerB_dv3_dc6_n1_bn2(0.00035, 0.035, 0.008);
print "Result of version 1: ".$nextp."\n";

$nextp = GallagerB_dv3_dc6_n1_bn2_version2(0.00035, 0.035, 0.008);
print "Result of version 2: ".$nextp."\n";

Math::BigFloat->accuracy(200);
my $p = new Math::BigFloat '0.00035';
my $p_o = new Math::BigFloat '0.035';
my $phi = new Math::BigFloat '0.008';
print "BigFloat p is ".$p."\n";
print "BigFloat p_o is ".$p_o."\n";
print "BigFloat phi is ".$phi."\n";
my $nextpBig = GallagerB_dv3_dc6_n1_bn2_version2($p, $p_o, $phi);
print "Result with BigFloat is: ".$nextpBig."\n";

my $pRat = Math::BigRat->new('35/100000');
my $poRat = Math::BigRat->new('35/1000');
my $phiRat = Math::BigRat->new('8/1000');
my $nextpRat = GallagerB_dv3_dc6_n1_bn2_version2($pRat, $poRat, $phiRat);
print "Result with BigRat is: ".$nextpRat."\n";


##################################################
#####               Functions                #####
##################################################

# Evaluate the EXIT function of the Gallager-B algorithm with d_v=3,
# d_c=6, n=1, b_n=2.
# Args:
#    $_[0] p_in
#    $_[1] p_o
#    $_[2] phi
# Returns:
#    p_out
sub GallagerB_dv3_dc6_n1_bn2 {
  my $p = shift;
  my $p_o = shift;
  my $phi = shift;

  my $nextp = ($phi + 5 *$p - 10 *$phi *$p + 40 *$phi *$p**2 - 80 *$phi *$p**3 
               + 80 *$phi *$p**4 
               - 32 *$phi *$p**5 - 20 *$p**2 + 40 *$p**3 - 40 *$p**4 + 16 *$p**5) * 
                 ($phi + 5 *$p + 2 *$p_o - 10 *$phi *$p - 2 *$phi *$p_o 
                  - 10 *$p *$p_o + 40 *$phi *$p**2 
                  - 80 *$phi *$p**3 + 80 *$phi *$p**4 - 32 *$phi *$p**5 
                  + 40 *$p**2 *$p_o 
                  - 80 *$p**3 *$p_o + 80 *$p**4 *$p_o - 32 *$p**5 *$p_o - 20 *$p**2 
                  + 40 *$p**3 
                  - 40 *$p**4 + 16 *$p**5 + 20 *$phi *$p *$p_o - 80 *$phi *$p**2 *$p_o 
                  + 160 *$phi *$p**3 *$p_o - 160 *$phi *$p**4 *$p_o 
                  + 64 *$phi *$p**5 *$p_o);

  return $nextp;
}

sub GallagerB_dv3_dc6_n1_bn2_version2 {
  my $p = shift;
  my $p_o = shift;
  my $Phi = shift;

my $nextp = (2048*$Phi*$p_o - 512*$p_o - 1024*$Phi - 2048*$Phi**2*$p_o 
             + 1024*$Phi**2 + 256)*$p**10 

               + (5120*$Phi + 2560*$p_o - 10240*$Phi*$p_o + 10240*$Phi**2*$p_o - 5120*$Phi**2 - 1280)*$p**9 
       + (23040*$Phi*$p_o - 5760*$p_o - 11520*$Phi - 23040*$Phi**2*$p_o 
          + 11520*$Phi**2 + 2880)*$p**8 
            + (15360*$Phi + 7680*$p_o - 30720*$Phi*$p_o + 30720*$Phi**2*$p_o 
               - 15360*$Phi**2 - 3840)*$p**7 
        + (26880*$Phi*$p_o - 6720*$p_o - 13440*$Phi - 26880*$Phi**2*$p_o + 13440*$Phi**2 + 3360)*$p**6 
          + (8032*$Phi + 4032*$p_o - 16128*$Phi*$p_o + 16128*$Phi**2*$p_o - 8064*$Phi**2 - 2000)*$p**5 
            + (6720*$Phi*$p_o - 1680*$p_o - 3280*$Phi - 6720*$Phi**2*$p_o + 3360*$Phi**2 + 800)*$p**4 
              + (880*$Phi + 480*$p_o - 1920*$Phi*$p_o + 1920*$Phi**2*$p_o - 960*$Phi**2 - 200)*$p**3 
                + (360*$Phi*$p_o - 90*$p_o - 140*$Phi - 360*$Phi**2*$p_o + 180*$Phi**2 + 25)*$p**2 
                  + (10*$Phi + 10*$p_o - 40*$Phi*$p_o + 40*$Phi**2*$p_o - 20*$Phi**2)*$p 
                    + 2*$Phi*$p_o - 2*$Phi**2*$p_o + $Phi**2;

  return $nextp;
}
