#include "../types.hpp"
#include "../AlgorithmFactory.hpp"
#include "../IAlgorithm.hpp"
#include "../State.hpp"

#include <memory>
#include <iostream>
#include <exception>
#include <math.h>

using std::shared_ptr;
using std::cout;
using std::endl;

//#define FLOAT_TOLERANCE 0.00000000001
#define FLOAT_TOLERANCE 0.000000001
//TODO: Always use checkEXIT2 instead of checkEXIT

bool checkEXIT(shared_ptr<IAlgorithm> a, metric_t pIn, metric_t pOut) {
  shared_ptr<State> pState(new State(0, pIn));
  metric_t pOut_actual = a->exitFunct(pState);
  if(pOut_actual != pOut_actual) return false;
  metric_t diff = pOut - pOut_actual;
  if(diff > FLOAT_TOLERANCE || diff < -FLOAT_TOLERANCE) return false;
  return true;
}

bool checkEXIT2(shared_ptr<IAlgorithm> a, metric_t pIn, metric_t pOut,
                uint digits) {
  // the comparison precision is given by "digits"
  double tol = pow(10, 0-static_cast<double>(digits));
  shared_ptr<State> pState(new State(0, pIn));
  metric_t pOut_actual = a->exitFunct(pState);
  if(pOut_actual != pOut_actual) return false;
  metric_t diff = pOut - pOut_actual;
  if(diff > tol || diff < -tol) return false;
  return true;
}

int main(int argc, char** argv) {

  AlgorithmFactory af(35, 1000, //p_o = 35/1000
                      8, 1000); //phi = 8/1000

  // Check the EXIT functions against pre-computed values
  // Check-values computed using "mupad" (Matlab makes mistakes?)
  try {
    shared_ptr<IAlgorithm> a1 = af.algorithmByParam(3,6,1,2);
    uint failCount = 0;
    if( !checkEXIT(a1,0.00035, 0.0007682288778)) failCount++;
    if( !checkEXIT(a1,0.0049, 0.003145825082)) failCount++;
    if( !checkEXIT(a1,0.0077, 0.004992585422)) failCount++;
    if( !checkEXIT(a1,0.01715, 0.01307933173)) failCount++;
    if( !checkEXIT(a1,0.02065, 0.0166978685)) failCount++;
    if( !checkEXIT(a1,0.03395, 0.03277865754)) failCount++;
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;
  }

  // Check-values computed using "maple"
  try {
    shared_ptr<IAlgorithm> a2 = af.algorithmByParam(3,6,2,3);
    uint failCount = 0;
    if( !checkEXIT(a2, 0, 0.00001526153216)) failCount++;
    if( !checkEXIT(a2, 0.0014, 0.0007489978890)) failCount++;
    if( !checkEXIT(a2, 0.0049, 0.002916782144)) failCount++;
    if( !checkEXIT(a2, 0.0077, 0.004971384936)) failCount++;
    if( !checkEXIT(a2, 0.00945, 0.006389871937)) failCount++;
    if( !checkEXIT(a2, 0.01715, 0.01372554604)) failCount++;
    if( !checkEXIT(a2, 0.0259, 0.02386726238)) failCount++;
    if( !checkEXIT(a2, 0.035, 0.03594730535)) failCount++;
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/8 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;
  }

  // Check-values computed using "maple"
  try {
    shared_ptr<IAlgorithm> a3 = af.algorithmByParam(3,6,2,4);
    uint failCount = 0;
    if( !checkEXIT(a3, 0, 0.001106635489)) failCount++;
    if( !checkEXIT(a3, 0.0035, 0.002546589823)) failCount++;
    if( !checkEXIT(a3, 0.0105, 0.006806962112)) failCount++;
    if( !checkEXIT(a3, 0.0189, 0.01396975833)) failCount++;
    if( !checkEXIT(a3, 0.02765, 0.02330902018)) failCount++;
    if( !checkEXIT(a3, 0.03325, 0.03007009433)) failCount++;
    if( !checkEXIT(a3, 0.035, 0.03228592565)) failCount++;
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/7 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;
  }

  // computed with mupad ("Faulty Gallager-B EXIT results.mn")
  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,1,2);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.000190976) ) failCount++;
    if( !checkEXIT(a, 0.0001, 0.0002251524595) ) failCount++;
    if( !checkEXIT(a, 0.0043, 0.003976905776) ) failCount++;
    if( !checkEXIT(a, 0.01, 0.01516118695) ) failCount++;
    if( !checkEXIT(a, 0.035, 0.1078327924) ) failCount++;
    if( !checkEXIT(a, 0.05, 0.1730795036) ) failCount++;
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  // computed with mupad ("Faulty Gallager-B EXIT results.mn")
  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,1,3);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.000833792) ) failCount++;
    if( !checkEXIT(a, 0.0001, 0.0009050102426) ) failCount++;
    if( !checkEXIT(a, 0.0043, 0.003778238197) ) failCount++;
    if( !checkEXIT(a, 0.01, 0.007482031162) ) failCount++;
    if( !checkEXIT(a, 0.035, 0.02553309749) ) failCount++;
    if( !checkEXIT(a, 0.05, 0.03897732404) ) failCount++;
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  // computed with maple ("Gallager-B message repetition 5.mw")
  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,3,5);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 4.019794707e-09, 14) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.000003393261281) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.002604831220) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.01261698644) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.1034490496) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.1688859266, 5) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,3,6);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 1.751380216e-08, 13) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.000004810563540) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.002554085540) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.01220877526) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.09957939691) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.1626329706, 5) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,3,7);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.000001451949366) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.00007322112259) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.003005353953) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.006884499125) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.02642781467) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.04095910862) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,3,8);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.00007768305589) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.0001509119082) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.003085910001) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.006817309436) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.02468200691) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.03801123513) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,3,9);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.002440847360) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.002509133049) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.005245591953) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.008723441528) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.02536692716) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.03778536208) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(6,12,1,3);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.000005058756608) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.000007387744375) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.001342356016) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.01010756523) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.1358843266) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.2283854578, 5) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(6,12,1,4);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.00002206338253) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.00002837650202) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0009055859630) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.003723741254) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.03752774117) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.06951289402) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(6,12,1,5);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.001377778516) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.001560627075) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.008285589496) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.01503005966) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.02977748918) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.03555787114) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(3,6,5,6);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 3.311927746e-10) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.00007037808842) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.003350670442) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.008727733673) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.04126131587) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.06478309693) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(3,6,5,7);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 2.896887509e-08, 13) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.00003580960889) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.001918006334) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.005560534683) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.03232692660) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.05350602310) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(3,6,5,8);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.000002061798147) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.00003728255647) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.001896828706) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.005511179902) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.03218303404) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.05332313000) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(3,6,5,9);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.00009658837033) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.0001317095145) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.001986051910) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.005590416144) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.03218948403) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.05327217858) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(3,6,5,10);
    uint failCount = 0;
    if( !checkEXIT(a, 0, 0.002701320581) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT(a, 0.0001, 0.002733820728) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.004449778684) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.007785159672) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT(a, 0.035, 0.03239921969) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT(a, 0.05, 0.05190857769) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  // Create an Algorithm Factory with different parameters
  AlgorithmFactory af2(18, 100, //p_o = 18/100
                       0, 1);   //phi = 0

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(12,24,1,6);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 1.170158882e-10, 15) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 5.151165276e-10, 15) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0002447005326) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.009215441131) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.2628589864,5) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.3834406664,5) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(12,24,1,7);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 5.096735132e-10, 15) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 1.748346293e-09, 14) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0001002692595) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.002891456389) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.1218376822, 5) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.2004544815, 4) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(12,24,1,8);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 4.523127599e-08, 13) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 1.207385830e-07, 12) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0005781921169) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.005112329595) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.05692671841, 6) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.09302232971, 3) ) {failCount++; cout<<"#6 fails"<<endl;}
    shared_ptr<IAlgorithm> a2 = af2.algorithmByParam(12,24,1,8);
    //if( !checkEXIT2(a2, 0.18, 0.2524775424, 3) ) {failCount++; cout <<"#7 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << " evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(12,24,1,9);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 0.000002818010724,11) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.000005861165872, 11) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.002889010702) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.01234065517) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.03775888065, 6) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.04902142244, 3) ) {failCount++; cout<<"#6 fails"<<endl;}

    shared_ptr<IAlgorithm> a2 = af2.algorithmByParam(12,24,1,8);
    //if( !checkEXIT2(a2, 0.18, 0.2009290786, 3) ) {failCount++; cout <<"#7 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << " evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  // (TODO)
  // try {
  //   shared_ptr<IAlgorithm> a = af.algorithmByParam(12,24,1,10);
  //   uint failCount = 0;
  //   if( !checkEXIT(a, 0, ) ) {failCount++; cout<<"#1 fails"<<endl;}
  //   if( !checkEXIT(a, 0.0001, ) ) {failCount++; cout<<"#2 fails"<<endl;}
  //   if( !checkEXIT(a, 0.0043, ) ) {failCount++; cout<<"#3 fails"<<endl;}
  //   if( !checkEXIT(a, 0.01, ) ) {failCount++; cout<<"#4 fails"<<endl;}
  //   if( !checkEXIT(a, 0.035, ) ) {failCount++; cout<<"#5 fails"<<endl;}
  //   if( !checkEXIT(a, 0.05, ) ) {failCount++; cout<<"#6 fails"<<endl;}
  //   if(failCount>0) {
  //     cout << "Test fails at line " << __LINE__;
  //     cout << " (" << failCount << "/6 evaluations fail)" << endl;
  //     return 1;
  //   }
  // } catch(std::exception&) {
  //   cout << "Exception caught at line " << __LINE__ << endl;
  //   return 1;    
  // }

  // try {
  //   shared_ptr<IAlgorithm> a = af.algorithmByParam(12,24,1,11);
  //   uint failCount = 0;
  //   if( !checkEXIT(a, 0, ) ) {failCount++; cout<<"#1 fails"<<endl;}
  //   if( !checkEXIT(a, 0.0001, ) ) {failCount++; cout<<"#2 fails"<<endl;}
  //   if( !checkEXIT(a, 0.0043, ) ) {failCount++; cout<<"#3 fails"<<endl;}
  //   if( !checkEXIT(a, 0.01, ) ) {failCount++; cout<<"#4 fails"<<endl;}
  //   if( !checkEXIT(a, 0.035, ) ) {failCount++; cout<<"#5 fails"<<endl;}
  //   if( !checkEXIT(a, 0.05, ) ) {failCount++; cout<<"#6 fails"<<endl;}
  //   if(failCount>0) {
  //     cout << "Test fails at line " << __LINE__;
  //     cout << " (" << failCount << "/6 evaluations fail)" << endl;
  //     return 1;
  //   }
  // } catch(std::exception&) {
  //   cout << "Exception caught at line " << __LINE__ << endl;
  //   return 1;    
  // }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,4,7);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 9.682490101e-12, 17) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.000001643032292, 15) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.002534013628) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.01246466849) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.1030626957,5) ) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.1684233814,5) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,4,8);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 8.668099583e-10, 15) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.000005808426461, 15) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.002560262010) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.01212502984) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.09844722092,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.1607517319, 5)) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,4,9);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 6.741350901e-08, 13) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.00007133753118, 10) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.003000707923) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.006923249268) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.02697528925,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.04187054929,5)) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,4,10);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 0.000003734876462, 10) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.00007710627408, 10) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.003018247537) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.006758627015) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.02467382408,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.03804034174,5) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,4,11);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 0.0001401644983, 8) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.0002132752653, 8) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.003143096251) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.006866704951) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.02468634745,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.03798236147,5) ) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(4,8,4,12);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 0.003216032337, 7) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.003282692279, 7) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.005954000363) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.009349044584) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.02559623919,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.03771898502,5)) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(6,12,3,8);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 1.027019905e-13, 18) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 3.883296948e-08, 13) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0009046063448) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.008607852870) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.1319351795,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.224883893,5)) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(6,12,3,9);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 4.471501983e-13, 18) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 4.125802913e-08, 13) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0008564068934) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.008128002439) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.1252203241,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.214142190,5)) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(6,12,3,10);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 4.316514093e-11, 16) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 4.238299218e-07, 12) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0006846020725) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.003499216196) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.04037763454,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.07490175730,5)) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  try {
    shared_ptr<IAlgorithm> a = af.algorithmByParam(6,12,3,11);
    uint failCount = 0;
    if( !checkEXIT2(a, 0, 3.221429609e-09, 14) ) {failCount++; cout<<"#1 fails"<<endl;}
    if( !checkEXIT2(a, 0.0001, 0.000001173561228, 12) ) {failCount++; cout<<"#2 fails"<<endl;}
    if( !checkEXIT(a, 0.0043, 0.0007013105026) ) {failCount++; cout<<"#3 fails"<<endl;}
    if( !checkEXIT(a, 0.01, 0.003317745646) ) {failCount++; cout<<"#4 fails"<<endl;}
    if( !checkEXIT2(a, 0.035, 0.03625565939,5)) {failCount++; cout<<"#5 fails"<<endl;}
    if( !checkEXIT2(a, 0.05, 0.06805403380,5)) {failCount++; cout<<"#6 fails"<<endl;}
    if(failCount>0) {
      cout << "Test fails at line " << __LINE__;
      cout << " (" << failCount << "/6 evaluations fail)" << endl;
      return 1;
    }
  } catch(std::exception&) {
    cout << "Exception caught at line " << __LINE__ << endl;
    return 1;    
  }

  cout << "(PASS)" << endl;
  return 0;
}
