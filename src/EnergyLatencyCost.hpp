//Author: Francois Leduc-Primeau

#ifndef EnergyLatencyCost_hpp_
#define EnergyLatencyCost_hpp_

#include "ICost.hpp"
#include "types.hpp"
#include <stdexcept>

/**
 * Cost defined as a pair of one positive real number and one positive integer.
 */
class EnergyLatencyCost : public ICost {
public:

  EnergyLatencyCost() :
    m_energy(0.0),
    m_latency(0),
    m_penalty(0.0)
  {}

  EnergyLatencyCost(double energy, uint latency)
    : m_energy(energy),
      m_latency(latency),
      m_penalty(0.0)
  {}

  /**
   * Calls the (implicit) copy constructor of the derived class and
   * returns a base class pointer.
   */
  ICost* getCopy() const { return new EnergyLatencyCost(*this); }

  double energy() const { return m_energy; }

  uint latency() const { return m_latency; }

  double penalty() const { return m_penalty; }  

  double oneDRepresentation() const { return m_energy + m_penalty; }

  /**
   * Adds another cost c2 to this one.
   *@throws std::bad_cast If c2 is not of type EnergyLatencyCost.
   */
  void operator+=(ICost const& c2) {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    m_energy+= elc2.energy();
    m_latency+= elc2.latency();
    m_penalty+= elc2.penalty();
  }

  void addPenalty(double newPenalty) { m_penalty+= newPenalty; }

  /**
   * Whether an ordering is defined between this cost and c2.
   *@throws std::bad_cast If c2 is not of type EnergyLatencyCost
   */
  bool isComparable(ICost const& c2) const {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    return isComparable(elc2);
  }

  bool isComparable(EnergyLatencyCost const& elc2) const {
    return (oneDRepresentation()<=elc2.oneDRepresentation() && m_latency<=elc2.latency())
      || (oneDRepresentation()>=elc2.oneDRepresentation() && m_latency>=elc2.latency());
  }

  ///@throws std::bad_cast If c2 is not of type EnergyLatencyCost.
  bool operator==(ICost const& c2) const {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    return oneDRepresentation()==elc2.oneDRepresentation() && m_latency==elc2.latency();
  }

  ///@throws std::bad_cast If c2 is not of type EnergyLatencyCost.
  bool operator!=(ICost const& c2) const {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    return oneDRepresentation()!=elc2.oneDRepresentation() || m_latency!=elc2.latency();
  }

  /**
   * Whether this cost is smaller than c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   *@throws std::bad_cast     if c2 is not of type EnergyLatencyCost.
   */
  bool operator<(ICost const& c2) const {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    return *this < elc2;
  }

  bool operator<(EnergyLatencyCost const& elc2) const {
    if(!isComparable(elc2)) throw std::logic_error("Costs are not comparable");
    return (oneDRepresentation()<elc2.oneDRepresentation() && m_latency<=elc2.latency())
        || (oneDRepresentation()<=elc2.oneDRepresentation() && m_latency<elc2.latency());
  }

  /**
   * Whether this cost is larger than c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   *@throws std::bad_cast     if c2 is not of type EnergyLatencyCost
   */
  bool operator>(ICost const& c2) const {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    return *this > elc2;
  }

  bool operator>(EnergyLatencyCost const& elc2) const {
    if(!isComparable(elc2)) throw std::logic_error("Costs are not comparable");
    return (oneDRepresentation()>elc2.oneDRepresentation() && m_latency>=elc2.latency())
        || (oneDRepresentation()>=elc2.oneDRepresentation() && m_latency>elc2.latency());
  }

  /**
   * Whether this cost is smaller or equal to c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   *@throws std::bad_cast     if c2 is not of type EnergyLatencyCost
   */
  bool operator<=(ICost const& c2) const {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    if(*this==elc2) return true;
    return *this < elc2;
  }

  /**
   * Whether this cost is larger or equal to c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   *@throws std::bad_cast     if c2 is not of type EnergyLatencyCost
   */
  bool operator>=(ICost const& c2) const {
    EnergyLatencyCost const& elc2 = dynamic_cast<EnergyLatencyCost const&>(c2);
    if(*this==elc2) return true;
    return *this > elc2;
  }

  std::ostream& toStream(std::ostream& stream) const {
    return stream << "("<<m_energy<<", "<<m_latency<<")";
  }

private:
  double m_energy;
  uint   m_latency;
  double m_penalty;
};

#endif
