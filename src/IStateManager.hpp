//Author: Francois Leduc-Primeau

#ifndef IStateManager_hpp_
#define IStateManager_hpp_

#include "State.hpp"
#include <memory>

class IStateManager {
public:
  
  virtual ~IStateManager() {}

  /**
   * Retrieve the best State that represents a metric equal with or worse than "p".
   */
  virtual std::shared_ptr<State> getState(metric_t p) = 0;

  /**
   * Retrieve the worst State that represents a metric better than "p".
   */
  virtual std::shared_ptr<State> getBetterState(metric_t p) = 0;

protected:
  IStateManager() {}
};

#endif
