#include "AlgorithmFactory.hpp"
#include "IAlgorithm.hpp"
#include "LOMSAlgorithm.hpp"

#include <exception>
#include <stdexcept>
#include <sstream>
#include <boost/filesystem.hpp>
#include <math.h>

using std::unique_ptr;
using std::shared_ptr;
using std::vector;
using std::string;
using boost::filesystem::path;

void AlgorithmFactory::init(string btwcRootPathString, vector<string> confList,
                            COSTDEF_t costdef)
{
  path rootPath(btwcRootPathString);
  // make sure the base path exists (note: file_exception is defined in config.hpp)
  if(!exists(rootPath)) throw file_exception(btwcRootPathString);

  double p_o;

  for(uint i=0; i<confList.size(); i++) {
    // combine the base path and the config file path
    path curConfPath = rootPath;
    curConfPath/= confList[i];

    // create a config object
    unique_ptr<config> curConf(new config());
    curConf->initFile(curConfPath.string()); // can throw a bunch of exceptions!

    // add to it the btwc root directory so that users of the config
    // object can actually resolve the other paths mentioned in it
    curConf->addConfElem("EXITtrellis2::btwc_root", btwcRootPathString);

    // check that all algorithms have the same channel parameter
    if(i==0) p_o = curConf->parseParamDouble("ch_dist");
    else {
      if(curConf->parseParamDouble("ch_dist") != p_o) {
	      throw std::runtime_error("All supplied ExitGen configs should have the same ch_dist");
      }
    }
    // save the channel distribution
    m_po = p_o;

    // Create the associated IAlgorithm
    m_costdef = costdef;
    shared_ptr<IAlgorithm> alg(new LOMSAlgorithm(std::move(curConf), costdef));
    m_algList.push_back(alg);
  }
}

AlgorithmFactory::~AlgorithmFactory() {
}
