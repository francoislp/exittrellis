//Author: Francois Leduc-Primeau

#ifndef Path_hpp_
#define Path_hpp_

#include "IAlgorithm.hpp"
#include "IStateManager.hpp"
#include <DE/DE.hpp>
#include "types.hpp"
#include <vector>
#include <memory>
#include <ostream>

/**
 * Represents a path through the trellis graph. It is composed of a
 * sequence of algorithm choices combined with a sequence of states
 * traversed.
 */
class Path {
public:

  /**
   * Construct a new path that will start from the state
   * specified. The cost Class is chosen by passing an object of that
   * class as "zeroCost".
   */
  Path(statep_t initialState, std::unique_ptr<cost_t> zeroCost,
       double algChangePenalty) {
    m_cost = std::move(zeroCost);
    m_stateSeq.push_back(initialState);
    m_algChangePenalty = algChangePenalty;
  }

  /// Copy constructor
  Path(const Path& other) { 
    m_cost = std::unique_ptr<cost_t>(other.getCost().getCopy());
    m_stateSeq = other.getStateSeq();
    m_algSeq = other.getAlgSeq();
    m_algChangePenalty = other.getAlgChangePenalty();
    m_DE= other.getDE();
  }

  /// Copy-assignment operator (same code as copy constructor)
  Path& operator=(const Path& other) {
    m_cost = std::unique_ptr<cost_t>(other.getCost().getCopy());
    m_stateSeq = other.getStateSeq();
    m_algSeq = other.getAlgSeq();
    m_algChangePenalty = other.getAlgChangePenalty();
    m_DE= other.getDE();
    return *this;
  }

  /**
   * Returns the number of edges in this Path, which is one less than
   * the number of States.
   */
  uint numberOfEdges()  const { return m_algSeq.size(); }

  cost_t const& getCost() const { return *m_cost; }

  /**
   * Extends this path to a new state, which is reached by using the
   * specified algorithm.
   *@param algorithm  The algorithm (rule) used to extend the path.
   *@param sm         The state manager that is used to retrieve the next state
   *                  object.
   */
	void extend(algp_t algorithm, IStateManager& sm) {
    statep_t curState = headState();
    statep_t nextState= sm.getState(algorithm->exitFunct(curState));
    m_stateSeq.push_back(nextState);
    m_algSeq.push_back(algorithm);
    (*m_cost)+= *(algorithm->getCost(curState));
    // if the algorithm is being changed, increase the penalty
    // associated with this path
    if(m_algChangePenalty>0 && m_algSeq.size()>1
       && algorithm!=m_algSeq[m_algSeq.size()-2]) {
      m_cost->addPenalty(m_algChangePenalty);
    }
  }

	/**
	 * Extends this path by adding one iteration of "algorithm". The next
	 * state is computed by performing density evolution on the entire path.
   *@param algorithm  The algorithm (rule) used to extend the path.
   *@param sm         The state manager that is used to retrieve the next state
   *                  object.
	 */
	void extendDE(algp_t algorithm, IStateManager& sm);

  /**
   * The sequence of States traversed by this path, starting with the
   * initial State. The length of this vector is always one more than
   * the length of the algorithm sequence.
   */
  std::vector<statep_t> getStateSeq() const { return m_stateSeq; }

  std::vector<algp_t> getAlgSeq() const { return m_algSeq; }

  /// Returns the last state reached by this Path.
  statep_t headState() const { return m_stateSeq[m_stateSeq.size()-1]; }

  double getAlgChangePenalty() const { return m_algChangePenalty; }

	bool operator==(Path const& p2) const {
		return *m_cost == p2.getCost() && *(headState()) == *(p2.headState());
	}

	bool operator!=(Path const& p2) const {
		return *m_cost != p2.getCost() || *(headState()) != *(p2.headState());
	}

	/// Whether this path makes p2 irrelevant.
	bool dominates(Path const& p2) const {
		if(!m_cost->isComparable(p2.getCost())) return false;
		// it does if its cost is smaller or equal and its head state is
		// "better or equal"
		return getCost() <= p2.getCost() && *(headState()) >= *(p2.headState());
	}

protected:
	DE::DE const& getDE() const { return m_DE; }

private:
  std::vector<statep_t> m_stateSeq;

  std::vector<algp_t> m_algSeq;

  /// Cost of this path.
  std::unique_ptr<cost_t> m_cost;

  /// Penalty to be added to the path cost when the algorithm is changed.
  double m_algChangePenalty;

	/// Density Evolution engine
	DE::DE m_DE;

};

std::ostream& operator<<(std::ostream& stream, const Path& p);

#endif
