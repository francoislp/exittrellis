//Author: Francois Leduc-Primeau

#include "EXITtrellis.hpp"
#include "AlgorithmFactory.hpp"
#include "COMMIT_ID.h"
#include "config/config.hpp"
#include "types.hpp"
#include "Path.hpp"
#include "ICost.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <regex>
#include <math.h>

using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::regex;
using std::regex_search;

// Run modes:
int normalRun(config& conf);
int checkInterpolRun(config& conf);


int main(int argc, char** argv) {

  // print usage if no arguments are provided
  if(argc==1) {
    cout << "Usage: EXITtrellis [key1=val1] [key2=val2] ..." << endl;
    cout << "To read the parameters from a file, use \"config=<path to config file>\""<<endl;
    return 1;
  }

  config conf;

  // specify the list of valid configuration keys
  conf.addValidKey("config");
  conf.addValidKey("btwc_root");
  conf.addValidKey("ExitGen_files");
  conf.addValidKey("po");
  conf.addValidKey("pres");
  conf.addValidKey("quant");
  conf.addValidKey("maxLatency");
  conf.addValidKey("cost_type");
  conf.addValidOption("printcmd");
  conf.addValidOption("printSequence");
  conf.addValidOption("printEXITdata");
  conf.addValidKey("interpolTestStep");
  conf.addValidKey("interpolTestEnd");
  conf.addValidKey("alg_change_penalty");
  conf.addValidKey("use_DE");

  // parse the command line arguments
  try {
    conf.initCL(argc, argv);
  } catch(syntax_exception& e) {
    cerr << "Invalid syntax in cmd-line arguments: "<<e.what()<<endl;
    return 1;
  } catch(invalidkey_exception& e) {
    cerr << "Invalid key: "<<e.what()<<endl;
    return 1;
  }

  // if a configuration file is specified, load its content
  try {
    std::string confpath = conf.getParamString("config");
    conf.initFile(confpath);
  } catch(key_not_found& e) {
    cerr << "Warning: No configuration file specified, using command line arguments only." << endl;
  } catch(invalidkey_exception& e) {
    cerr << "Invalid key: "<<e.what()<<endl;
    return 1;
  } catch(file_exception& e) {
    cerr << "Error reading from file at " << e.what() << endl;
    return 1;
  } catch(syntax_exception& e) {
    cerr << "Invalid syntax: "<<e.what() << endl;
    return 1;
  }

  // print git-commit id
  cout << "# Git commit SHA1: " << GIT_COMMIT_SHA1 << endl;

  // print command to std out
  if( conf.checkOption("printcmd") ) {
    cout << "#";
    for(int i=0; i<argc; i++) cout << " " << argv[i];
    cout << endl;
  }

  int retval = 1;
  if(conf.checkOption("printEXITdata")) {
    try {
      retval = checkInterpolRun(conf);
    } catch(std::exception& e) {
      cerr << "Exception was thrown:" << endl;
      cerr << e.what() << endl;
    }
    return retval;
  }

  try {
    retval = normalRun(conf);
  } catch(std::exception& e) {
    cerr << "Exception was thrown:" << endl;
    cerr << e.what() << endl;
  }
  return retval;
}

int normalRun(config& conf) {
  // Mandatory parameters:
  string         btwcRootPath;
  vector<string> exitFileList;
  metric_t       po, pres;
  uint           stateQuant;
  COSTDEF_t      costdef;
  try {
    btwcRootPath = conf.getParamString("btwc_root");
    if(!conf.listParser("ExitGen_files", exitFileList)) {
      cerr << "Invalid list syntax for \"ExitGen_files\"" <<endl;
      return 1;
    }
    po   = conf.parseParamDouble("po"); //TODO: should use a templatized function
    pres = conf.parseParamDouble("pres"); //TODO: should use a templatized function
    stateQuant = conf.parseParamUInt("quant");
    string costTypeName = conf.getParamString("cost_type");
    if(costTypeName == "energy" || costTypeName == "ENERGY") {
      costdef = ENERGY;
    } else if(costTypeName == "energy-latency" || costTypeName == "ENERGY-LATENCY") {
      costdef = ENERGY_LATENCY;
    } else if(costTypeName == "edp" || costTypeName == "EDP") {
      costdef = EDP;
    } else {
      cerr << "Invalid value for \"cost_type\": "<<costTypeName<<endl;
      return 1;
    }
  } catch(key_not_found& e) {
    cerr << "Missing mandatory key \" "<<e.what()<<" \""<<endl;
    return 1;
  }
  // Optional parameters:
  uint maxLatency = 0;
  try {
    maxLatency = conf.parseParamUInt("maxLatency");
  } catch(key_not_found&) {
    if(costdef==ENERGY_LATENCY) {
      cerr << "\"maxLatency\" must be specified when \"cost_type\"=energy-latency"<<endl;
      return 1;
    }
  }
  double algChangePenalty = 0;
  try {
    algChangePenalty = conf.parseParamDouble("alg_change_penalty");
  } catch(key_not_found&) {}
  bool useDE= false;
  try {
	  useDE= conf.parseParamBool("use_DE");
  } catch(key_not_found&) {}

  // Check for options
  bool doPrintSequence = conf.checkOption("printSequence");

  try {
    // Create an AlgorithmFactory
    AlgorithmFactory af(btwcRootPath, exitFileList, costdef);

    // Create an Optimization engine
    EXITtrellis engine1(stateQuant, af, po, pres);

    bool success = engine1.run(maxLatency, algChangePenalty, useDE);

    // Actual conditions used by the optimizer
    cout << "Quantized initial state was p="<<engine1.getQuantInitState()<<endl;
    cout << "Quantized objective state was p="<<engine1.getQuantObjState()<<endl;

    if(success) {
      Path& optPath = engine1.getOptimalPath();
      cout << "The best path arrives at state p="<<optPath.headState()->getMetric();
      cout << " with cost= "<< optPath.getCost();
      cout << " (virtual 1-D cost="<<optPath.getCost().oneDRepresentation()<<")";
      cout << endl;

      if(doPrintSequence) {
        vector<algp_t> algList = optPath.getAlgSeq();
        vector<statep_t> stateList = optPath.getStateSeq();
        cout << "The algorithm sequence: " << endl;
        cout << "t=0: p="<<stateList[0]->getMetric() << endl;
        for(uint i=0; i<algList.size(); i++) {
          cout << "t="<<(i+1) <<": ";
          cout << algList[i]->getName() << ", ";
          cout << "p_out="<<stateList[i+1]->getMetric();
          // also print the energy used in each iteration
          cout << ", E="<<algList[i]->getCost(stateList[i])->energy();
          cout << endl;
        }
      }
    } else {
      cout << "*Could not find a solution*" << endl;
    }
  } catch(key_not_found& e) {
    cerr << "A mandatory key is missing in one of the ExitGen configuration files:";
    cerr << e.what() << endl;
    return 1;
  } catch(syntax_exception& e) {
    cerr << "Syntax error in one of the ExitGen configuration file." << endl;
    cerr << "Invalid text: "<<e.what()<<endl;
    return 1;
  } catch(file_exception& e) {
    cerr << "File "<<e.what()<<" was not found or could not be opened."<<endl;
    return 1;
  }

  return 0;
}

int checkInterpolRun(config& conf) {
  string         btwcRootPath;
  vector<string> exitFileList;
  metric_t       po;
  metric_t       increment;
  metric_t       rangeEnd;
  try {
    btwcRootPath = conf.getParamString("btwc_root");
    if(!conf.listParser("ExitGen_files", exitFileList)) {
      cerr << "Invalid list syntax for \"ExitGen_files\"" <<endl;
      return 1;
    }
    po   = conf.parseParamDouble("po"); //TODO: should use a templatized function
    rangeEnd = conf.parseParamDouble("interpolTestEnd");
    increment = conf.parseParamDouble("interpolTestStep");
  } catch(key_not_found& e) {
    cerr << "Missing mandatory key \" "<<e.what()<<" \""<<endl;
    return 1;
  }

  try {
    // Create an AlgorithmFactory
    AlgorithmFactory af(btwcRootPath, exitFileList);

    for(uint i=0; i<af.algorithmListSize(); i++) {
      cout << "# Interpolated EXIT curve for algorithm #"<<(i+1)<<endl;
      algp_t alg = af.algorithmByIndex(i);
      for(metric_t curP=po; curP>rangeEnd; curP-=increment) {
        statep_t curState(new State(0, curP));
        cout << curP << "   " << alg->exitFunct(curState);
        cout << "   " << *(alg->getCost(curState)) << endl;
      }
      // make sure we include the end point
      statep_t curState(new State(0, rangeEnd));
      cout << rangeEnd << "   " << alg->exitFunct(curState);
      cout << "   " << *(alg->getCost(curState)) << endl;
      cout << endl;
    }

  } catch(key_not_found& e) {
    cerr << "A mandatory key is missing in one of the ExitGen configuration files:";
    cerr << e.what() << endl;
    return 1;
  } catch(syntax_exception& e) {
    cerr << "Syntax error in one of the ExitGen configuration file." << endl;
    cerr << "Invalid text: "<<e.what()<<endl;
    return 1;
  } catch(file_exception& e) {
    cerr << "File "<<e.what()<<" was not found or could not be opened."<<endl;
    return 1;
  }

  return 0;
}
