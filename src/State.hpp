#ifndef State_hpp_
#define State_hpp_

#include "types.hpp"
#include "IAlgorithm.hpp"
//#include "Path.hpp"
#include <memory>
#include <vector>

class State {
public:

  /**
   * Creates a new state with ID id representing value p of the
   * objective function. The ID must be a unique identifier, but is
   * also used to define an ordering of the States. "Better" states
   * must have higher ID values.
   */
  State(stateid_t id, metric_t p);

  // /**
  //  * Forget all paths that were registered.
  //  */
  // void clear() { m_pathList.clear(); }

  bool operator==(State const& s2) const { return (m_ID==s2.m_ID); }

  bool operator!=(State const& s2) const { return (m_ID!=s2.m_ID); }

  bool operator<(State const& s2) const { return (m_ID<s2.m_ID); }

  bool operator>(State const& s2) const { return (m_ID>s2.m_ID); }

  bool operator<=(State const& s2) const { return (m_ID<=s2.m_ID); }

  bool operator>=(State const& s2) const { return (m_ID>=s2.m_ID); }

  // /**
  //  * Registers a path that reaches this State.
  //  *@return False if the path was discarded because it is worse than existing paths 
  //  *        that reach this State, and true otherwise.
  //  */
  // bool reach(Path& path);

  stateid_t getID() const { return m_ID; }

  metric_t getMetric() const { return m_p; }

  // /**
  //  * Whether this state is reached by at least one path.
  //  */
  // bool isReachable() const { return m_pathList.size()>0; }

  // /**
  //  * Returns all the paths that have been registered with this State
  //  * (these paths all reach this State).
  //  */
  // std::vector<Path> const& getPaths() const { return m_pathList; }

  // /**
  //  * Adds the paths that have been registered with this State to the
  //  * provided vector (these paths all reach this State).
  //  */
  // std::vector<Path>& copyPaths(std::vector<Path>& returnList) {
  //   for(uint i=0; i<m_pathList.size(); i++) returnList.push_back(m_pathList[i]);
  //   return returnList;
  // }

private:

  stateid_t m_ID;

  metric_t m_p;

	//std::vector<Path> m_pathList;

};

#endif
