#ifndef types_hpp_
#define types_hpp_

typedef unsigned int uint;

/// Unique identifier for States
typedef uint stateid_t;

/// "Cost" of a path
#include "ICost.hpp"
typedef ICost cost_t;

/// A value of the objective function
typedef double metric_t;

#include <memory>
class State;
class IAlgorithm;
typedef std::shared_ptr<State> statep_t;
typedef std::shared_ptr<IAlgorithm> algp_t;

/// Enumerates the subclasses of ICost.
enum COSTDEF_t {ENERGY, ENERGY_LATENCY, EDP};

#endif
