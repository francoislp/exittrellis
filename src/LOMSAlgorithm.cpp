#include "LOMSAlgorithm.hpp"
#include "ExitResultParser.hpp"
#include "EnergyCost.hpp"
#include "EnergyLatencyCost.hpp"
#include "EDPCost.hpp"
#include <boost/filesystem.hpp>
#include <math.h>
#include <exception>
#include <stdexcept>
#include <sstream>

using std::unique_ptr;
using std::string;
using std::vector;
using boost::filesystem::path;
using std::stringstream;

LOMSAlgorithm::LOMSAlgorithm(unique_ptr<config> conf, COSTDEF_t costdef) {
	m_conf= std::unique_ptr<config>(std::move(conf));
  m_algName = m_conf->getFileName();

  // load the EXIT data
  string btwcRootPathString = m_conf->getParamString("EXITtrellis2::btwc_root");
  path exitFilePath(btwcRootPathString);
  exitFilePath/= m_conf->getParamString("exit_filepath");

  m_costdef = costdef;
  if(costdef==ENERGY_LATENCY || costdef==EDP) {
    // load the latency for this algorithm (normalized per check node)
    m_latency = m_conf->parseParamUInt("Tclk");
  }

  ExitResultParser parser;
  parser.parseFile(exitFilePath.string());

  // copy data back from parser
  vector<metric_t> pInList  = parser.getPInList();
  vector<metric_t> pOutList = parser.getPOutList();
  vector<double>   energyList = parser.getEnergyList();
  for(uint i=0; i<pInList.size(); i++) {
    if(pOutList[i]>0) {
      m_exitPoints[ pInList[i] ] = pOutList[i];
      m_originalXValsExit.insert(pInList[i]);
    }
    m_energyPoints[ pInList[i] ] = energyList[i];
    m_originalXValsEnergy.insert(pInList[i]);
  }
}

//NOTE: Similar code in LOMSAlgorithm::getCost()
metric_t LOMSAlgorithm::exitFunct(statep_t curState) {
  metric_t curpIn = curState->getMetric();
  // check if the point requested is already available
  auto it = m_exitPoints.find(curpIn);
  if(it!=m_exitPoints.end()) return it->second;

  // otherwise add a new point by interpolating from existing data
  auto it2 = m_exitPoints.begin();
  auto itEnd = m_exitPoints.end();
  metric_t xH,yH,xL,yL; // two points that will be used to interpolate

  if(it2==itEnd) throw std::exception(); // should not happen
  if(it2->first > curpIn) {
    // point requested is smaller than lower bound of characterized range
    // => interpolate using the two smallest values from MC data
    auto itMC = m_originalXValsExit.begin();
    xL = *itMC;
    itMC++;
    xH = *itMC;
    yL = m_exitPoints[xL];
    yH = m_exitPoints[xH];
  } else if((--itEnd)->first < curpIn) {
    // point requested is larger than upper bound of characterized range
    // => interpolate using the two largest values from MC data
    auto itMC = m_originalXValsExit.end();
    itMC--;
    xH = *itMC;
    itMC--;
    xL = *itMC;
    yL = m_exitPoints[xL];
    yH = m_exitPoints[xH];
  } else { // point requested is within characterized range
    while(it2->first < curpIn && it2!=m_exitPoints.end()) {
      it2++;
    }
    // if we reached the last element and it is still not larger than
    // curpIn we have a bug
    if(it2==m_exitPoints.end()) throw std::exception();

    // now it2->first points to the first available point that is larger
    // than what we want
    xH = it2->first;
    yH = it2->second;
    it2--;
    xL = it2->first;
    yL = it2->second;
  }

  // compute a log-linear interpolation
  metric_t a = (log(yH)-log(yL)) / (log(xH) - log(xL));
  metric_t b = log(yL)-a*log(xL);
  metric_t pOut = pow(curpIn,a)*exp(b);
  m_exitPoints[curpIn] = pOut; // save the interpolation result
  return pOut;
}

//NOTE: Code similar to LOMSAlgorithm::exitFunct().
std::unique_ptr<cost_t> LOMSAlgorithm::getCost(statep_t curState) {
  metric_t curpIn = curState->getMetric();
  // check if the point requested is already available
  auto it = m_energyPoints.find(curpIn);
  if(it!=m_energyPoints.end()) {
    if(m_costdef==ENERGY_LATENCY) {
      return unique_ptr<cost_t>(new EnergyLatencyCost(it->second, m_latency));
    } else if(m_costdef==EDP) {
      return unique_ptr<cost_t>(new EDPCost(it->second, m_latency));
    } else {
      return unique_ptr<cost_t>(new EnergyCost(it->second));
    }
  }

  // otherwise add a new point by interpolation from existing data
  auto it2 = m_energyPoints.begin();
  auto itEnd = m_energyPoints.end();
  metric_t xH,xL; // two points that will be used to interpolate
  double   yH,yL;

  if(it2==m_energyPoints.end()) throw std::exception(); // should not happen
  if(it2->first > curpIn) {
    // point requested is smaller than lower bound of characterized range
    // => interpolate using the two smallest values from MC data
    auto itMC = m_originalXValsEnergy.begin();
    xL = *itMC;
    itMC++;
    xH = *itMC;
    yL = m_energyPoints[xL];
    yH = m_energyPoints[xH];
  }
  else if((--itEnd)->first < curpIn) {
    // point requested is larger than upper bound of characterized range
    // => interpolate using the two largest values from MC data
    auto itMC = m_originalXValsEnergy.end();
    itMC--;
    xH = *itMC;
    itMC--;
    xL = *itMC;
    yL = m_energyPoints[xL];
    yH = m_energyPoints[xH];
  } else { // point requested is within characterized range
   while(it2->first < curpIn && it2!=m_energyPoints.end()) {
     it2++;
   }
   // if we reached the last element and it is still not larger than
   // curpIn we have a bug
   if(it2==m_energyPoints.end()) throw std::exception();

   // now it2->first points to the first available point that is larger
   // than what we want
   xH = it2->first;
   yH = it2->second;
   it2--;
   xL = it2->first;
   yL = it2->second;
  }

  // compute a semi-log interpolation
  metric_t a = (yH - yL) / (log(xH) - log(xL));
  //metric_t b = yL - a * log(xL);
  double curEnergy = a*(log(curpIn)-log(xL)) + yL;

  // sanity check: cost should be strictly positive
  if(curEnergy<=0) throw std::runtime_error("Energy cost should be strictly positive");

  m_energyPoints[curpIn] = curEnergy; // save the interpolation result
  if(m_costdef==ENERGY_LATENCY) {
    return unique_ptr<cost_t>(new EnergyLatencyCost(curEnergy, m_latency));
  } else if(m_costdef==EDP) {
    return unique_ptr<cost_t>(new EDPCost(curEnergy, m_latency));
  } else {
    return unique_ptr<cost_t>(new EnergyCost(curEnergy));
  }
}
