#ifndef StateManagerLog_hpp_
#define StateManagerLog_hpp_

#include "types.hpp"
#include "IStateManager.hpp"
#include "State.hpp"
#include <memory>
#include <map>

/**
 * Manages a collection of discretized states, distributed according
 * to a logarithmic distribution. State objects are created in a lazy
 * fashion.  The current implementation assumes several things: The
 * metrics are >0 and <=1, and a smaller metric is a "better" metric.
 */
class StateManagerLog : public IStateManager {
public:

  /**
   * Creates a discretized state space with logarithmically
   * distributed points.
   *@param nbPointsDec Quantization precision as the number of points per decade.
   *@param worseMetric The worse metric that will need to be represented by a 
   *       State. This is currently ignored, but an exception will be thrown if 
   *       not >0 and <=1.
   */
  StateManagerLog(uint nbPointsDec, metric_t worseMetric);

  ~StateManagerLog();

  ///@see IStateManager::getState(metric_t)
  statep_t getState(metric_t p);

  ///@see IStateManager::getBetterState(metric_t)
  statep_t getBetterState(metric_t p);

private:

  uint m_nbPointsDec;

  std::map<stateid_t, statep_t > m_stateMap;

};

#endif
