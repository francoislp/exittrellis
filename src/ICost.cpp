#include "ICost.hpp"

std::ostream& operator<<(std::ostream& stream, const ICost& c) {
  return c.toStream(stream);
}
