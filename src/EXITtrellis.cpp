//Author: Francois Leduc-Primeau

//#define DEBUG
//#define DEBUG2

#include "EXITtrellis.hpp"
#include "IStateManager.hpp"
#include "StateManagerLog.hpp"
//#include "State.hpp"
#include "EnergyCost.hpp"
#include "EnergyLatencyCost.hpp"

#include <list>
#include <iostream>
using std::cerr;
using std::endl;
#include <algorithm>
#include <math.h>

using std::shared_ptr;
using std::vector;


// struct SortPathsByHead {
// 	bool operator()(Path const& p1, Path const& p2) {
// 		return *(p1.headState()) < *(p2.headState());
// 	}
// };

EXITtrellis::EXITtrellis(uint statesPerDecade, AlgorithmFactory& af)
  : m_af(af) 
{
  m_statesPerDecade = statesPerDecade;
  m_optimizationIterLimit = ITERLIMIT_DEFAULT;
  // set the parameters to invalid values
  setPo(0);
  setObjective(1);
}

EXITtrellis::EXITtrellis(uint statesPerDecade, AlgorithmFactory& af,
                         metric_t p_o, metric_t p_res) 
  : m_af(af)
{
  m_statesPerDecade = statesPerDecade;
  m_optimizationIterLimit = ITERLIMIT_DEFAULT;
  setPo(p_o);
  setObjective(p_res);
}

bool EXITtrellis::run(uint maxLatency, double algChangePenalty,
                      bool useDE) {

  // reset the results
  m_pOptPath = std::unique_ptr<Path>();
  m_quantInit = 0;
  m_quantObj = 0;

  // Make sure that the AlgorithmFactory has the right channel parameter
  // (should correspond to the optimization's start value m_po)
  if(m_af.getPo() != m_po) {
	  throw std::runtime_error("Start of optimization does not match channel distribution of algorithms!");
  }

  // States are accessed through a State Manager
  StateManagerLog sm(m_statesPerDecade, // nbr of pts/decade
                     1); // worse metric (ignored)

  statep_t pInitialState = sm.getState(m_po);
  m_quantInit = pInitialState->getMetric();
  // Desired terminal state
  statep_t pObjTerminalState = sm.getBetterState(m_pres);
  m_quantObj = pObjTerminalState->getMetric();

  std::list<Path> workList;
  workList.push_back(Path(pInitialState, std::move(m_af.getCostClass()),
                          algChangePenalty));
  vector<Path> candidateList;

  // The number of available decoding rules is constant
  uint ruleCount = m_af.algorithmListSize();
  // Make sure at least one rule is available
  if(ruleCount<1) {
    throw std::runtime_error("No algorithms available");
  }  

  while(workList.size()>0) {
#ifndef DEBUG2
	  if(useDE) {
#endif
	  cerr<< "path list size before purge: "<<workList.size();
#ifndef DEBUG2
	  }
#endif
	  // purge path list
	  // 1. check if some paths that achieve the objective dominate some
	  //    paths in the work list
	  for(auto itTerm= candidateList.begin();
	      itTerm!=candidateList.end(); itTerm++) {
		  auto it= workList.begin();
		  while(it != workList.end()) {
			  if(itTerm->dominates(*it)) it= workList.erase(it);
			  else it++;
		  }
	  }
	  // 2. check if some paths dominate others within the work list
	  auto it= workList.begin();
	  while(it != workList.end()) {
		  auto it2= workList.begin();
		  while(it2 != workList.end()) {
			  //Note: erase(.) returns iterator to element after erased one
			  if(it!=it2 && it->dominates(*it2)) it2= workList.erase(it2);
			  else it2++;
		  }
		  it++;
	  }
#ifndef DEBUG2
	  if(useDE) {
#endif
	  cerr<< ", after: "<<workList.size() <<endl;
#ifndef DEBUG2
	  }
#endif

	  // extend paths
	  std::list<Path> nextList;
	  for(it= workList.begin(); it!=workList.end(); it++) {
		  // if path achieves objective, add it to the list of candidate solutions
		  if(*(it->headState()) >= *(pObjTerminalState)) {
			  candidateList.push_back(*it);
		  }
		  else { // extend it
			  for(uint j=0; j<ruleCount; j++) {
				  algp_t pCurAlg = m_af.algorithmByIndex(j);
				  Path curPath= *it; // make a copy
				  if(useDE) curPath.extendDE(pCurAlg, sm);
				  else curPath.extend(pCurAlg, sm);
				  // only add the new path if it makes progress and doesn't
				  // exceed the latency constraint (if there is one)
				  if(*(curPath.headState()) > *(it->headState())
				     && (maxLatency<=0 ||
				         curPath.getCost().latency() <= maxLatency)) {
					  nextList.push_back(curPath);
				  }
			  }
		  }
	  }
	  workList= nextList;
  }

  // check if we found some solutions
  if( candidateList.size() == 0 ) {
	  return false;
  }
  
#ifdef DEBUG2
  cerr<<candidateList.size()<<" solutions found: "<<endl;
  for(uint i=0; i<candidateList.size(); i++) {
	  statep_t pCurS = candidateList[i].headState();
    cost_t const& curCost = candidateList[i].getCost();
    cerr << "p="<<pCurS->getMetric() << ", cost="<<curCost;
    cerr << ", 1-D cost="<<curCost.oneDRepresentation() << endl;
  }
#endif
  
  // pick the path with the lowest cost (in case of a tie pick a path
  // with a better head state)
  double minCost = candidateList[0].getCost().oneDRepresentation();
  m_pOptPath = std::unique_ptr<Path>(new Path(candidateList[0]));
  for(uint i=1; i<candidateList.size(); i++) {
	  double cur1DCost= candidateList[i].getCost().oneDRepresentation();
    if( cur1DCost < minCost
        || (cur1DCost==minCost
            && *(candidateList[i].headState()) > *(m_pOptPath->headState()))) {
	    minCost = cur1DCost;
      *m_pOptPath = candidateList[i];
    }
  }
  return true;
}
