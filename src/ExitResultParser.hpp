//Author: Francois Leduc-Primeau

#ifndef ExitResultParser_hpp_
#define ExitResultParser_hpp_

#include "types.hpp"
#include <string>
#include <fstream>
#include <vector>


class ExitResultParser {
public:

  ExitResultParser() {}

  /**
   * Parses the file specified and copies its content to memory.
   *@throws file_exception   If the file cannot be opened.
   *@throws syntax_exception If there is an error while parsing.
   *@throws sanity_exception If some values are outside the allowed ranges.
   */
  void parseFile(std::string filepath);

  std::vector<metric_t>& getPInList() { return m_pInList; }

  /**
   * Returns the list of p_out values (2nd column) found in the EXIT
   * result file. An EXIT result file can omit specifying certain
   * p_out values by using the keyword "N/A". If a value is omitted
   * the value is set to -1.
   */
  std::vector<metric_t>& getPOutList() { return m_pOutList; }

  std::vector<double>& getEnergyList() { return m_energyList; }

private:

  /// Returns a line from the ifstream while skipping over comment lines.
  void getline_nc(std::ifstream&, std::string&);

  // ----- Data -----
  std::vector<metric_t> m_pInList;
  std::vector<metric_t> m_pOutList;
  std::vector<double>   m_energyList;
};

#endif
