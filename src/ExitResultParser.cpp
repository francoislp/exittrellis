//Author: Francois Leduc-Primeau

#include "ExitResultParser.hpp"
#include "config/config.hpp" // for file_exception, syntax_exception
#include <stdexcept>
#include <fstream>
#include <sstream>

// Character indicating a line comment
#define COMMENTCHAR '#'

using std::string;
using std::vector;
using std::ifstream;
using std::stringstream;

void ExitResultParser::parseFile(std::string filepath) {
  ifstream ifs(filepath.c_str());
  if(!ifs.good()) throw file_exception(filepath);

  string curLine;
  while(ifs.good()) {
    getline_nc(ifs, curLine);
    if(curLine != "") {
      stringstream ss;
      ss << curLine;

      metric_t curVal;
      // extract p_in
      ss >> curVal;
      if(ss.fail()) throw syntax_exception(curLine);
      // perform some checks on the value
      if(curVal<=0 || curVal >=1) throw std::runtime_error("p_in check fails");
      m_pInList.push_back(curVal);

      // extract p_out
      ss >> curVal;
      if(ss.fail()) {
        // if that fails, check if the value is the string "N/A"
        ss.clear(); // clear error flags
        ss.seekg(-1, std::ios_base::cur); // move cursor back
        string str;
        ss >> str;
        if(str=="N/A") m_pOutList.push_back(-1);
        else throw syntax_exception(curLine);
      } else {
        if(curVal<=0 || curVal >=1) 
          throw std::runtime_error("p_out check fails");
        m_pOutList.push_back(curVal);
      }

      double curEnergy;
      // extract energy cost
      ss >> curEnergy;
      if(ss.fail()) throw syntax_exception(curLine);
      if(curEnergy<=0) throw std::runtime_error("energy check fails");
      m_energyList.push_back(curEnergy);
    }
  }
}

// Private

void ExitResultParser::getline_nc(ifstream& ifs, string& line) {
  char firstchar = COMMENTCHAR;

  while(firstchar == COMMENTCHAR) {
    if(!ifs.eof()) std::getline(ifs, line);
    else line = "";
    stringstream sstream;
    sstream << line;
    sstream >> std::ws; // extract leading whitespace
    firstchar = sstream.peek();
  }
}
