//Author: Francois Leduc-Primeau

#ifndef EXITtrellis_hpp_
#define EXITtrellis_hpp_

#include "IAlgorithm.hpp"
#include "AlgorithmFactory.hpp"
#include "Path.hpp"
#include "types.hpp"
#include <memory>
#include <vector>

/// Default value for the number of states to be processed before giving up.
#define ITERLIMIT_DEFAULT 10000000

/**
 * Finds the optimal sequence of decoding rules for specific parameters.
 */
class EXITtrellis {
public:
  /**
   * Creates a new instance with uninitialized parameters.
   *@param statesPerDecade  Precision of the State quantization.
   *@param af               AlgorithmFactory that defines the available decoding 
   *                        "rules" or "algorithms".
   */
  EXITtrellis(uint statesPerDecade, AlgorithmFactory& af);

  EXITtrellis(uint statesPerDecade, AlgorithmFactory& af,
              metric_t p_o, metric_t p_res);

  /**
   * Sets the starting point of the optimization, which must
   * correspond to the p_o of the AlgorithmFactory.
   */
  void setPo(metric_t p_o) { m_po = p_o; }

  void setObjective(metric_t p_res) { m_pres = p_res; }

  /**
   * Run the optimization without a latency constraint. The result can
   * be retrieved using getOptimalPath().
   *@return True if a solution was found.
   */
	bool run() { return run(0, 0, false); }

  /**
   * Run the optimization with the specified latency constraint. The
   * result can be retrieved using getOptimalPath().
   *
   *@param maxLatency The latency constraint for achieving the
   *       residual error rate. If the value is 0 no constraint is used.
   *@param algChangePenalty Penalty that is added to the path cost when 
   *       the algorithm is changed.
   *@param useDE  Whether to compute the message error rate using density
   *              evolution rather than the ExIT function.
   *
   *@return True if a solution was found.
   */
	bool run(uint maxLatency, double algChangePenalty, bool useDE);

  // Info concerning the conditions and the results of the optimization:

  /**
   * Returns the optimal path found during the last call to
   * run(). Result is undefined if run() has not been called or did
   * not return true.
   */
  Path& getOptimalPath() { return *m_pOptPath; }

  /**
   * Returns the quantized initial state metric that was used for the
   * last optimization run.
   */
  metric_t getQuantInitState() { return m_quantInit; }

  /**
   * Returns the quantized metric that was used as the objective for
   * the last optimization run.
   */
  metric_t getQuantObjState() { return m_quantObj; }

private:

  // ----- Data -----
  AlgorithmFactory& m_af;

  metric_t m_po;
  metric_t m_pres;

  uint m_statesPerDecade;

  /**
   * Maximum number of states to be processed, after which we stop
   * looking for solutions.
   */
  uint m_optimizationIterLimit;

  // Optimization results:
  std::unique_ptr<Path> m_pOptPath; // optimal path
  metric_t m_quantInit; // quantized initial metric
  metric_t m_quantObj; // quantized objective metric
};

#endif
