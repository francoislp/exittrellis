//Author: Francois Leduc-Primeau

#ifndef IAlgorithm_hpp_
#define IAlgorithm_hpp_

#include "types.hpp"
#include "config/config.hpp"
#include <memory>
#include <string>

class State; // forward declaration

class IAlgorithm {
public:

  virtual ~IAlgorithm() {}

  /// Returns the unique name of the algorithm.
  virtual std::string getName() const = 0;

	/// Returns the configuration for this algorithm.
	virtual config const& getConfig() const = 0;

  /**
   * Evaluates the cost function associated with this algorithm at the
   * given state.
   */
  virtual std::unique_ptr<cost_t> getCost(statep_t curState) = 0;

  /**
   * Evaluates the EXIT function associated with this algorithm at
   * the given state, and returns the resulting metric.
   */
  virtual metric_t exitFunct(statep_t curState) = 0;

  /**
   * Returns true if a2 has the same name as this one.
   */
  virtual bool operator==(IAlgorithm const& a2) const =0;

  /**
   * Returns true if a2 has a different name than this one.
   */
  virtual bool operator!=(IAlgorithm const& a2) const =0;  

protected:
  IAlgorithm() {}

};

#endif
