#include "StateManagerLog.hpp"

#include <math.h>
#include <exception>
#include <stdexcept>
#ifdef DEBUG
#include <iostream>
using std::cerr;
using std::endl;
#endif


StateManagerLog::StateManagerLog(uint nbPointsDec, metric_t worseMetric) 
  : m_nbPointsDec(nbPointsDec)
{
  if(worseMetric<=0 || worseMetric>1) throw std::exception();
}

StateManagerLog::~StateManagerLog() {
#ifdef DEBUG
  cerr << "StateManagerLog desctructor called" << endl;
  cerr << "  contains "<<m_stateMap.size()<<" states" << endl;
#endif
}

//Note: Similar code in StateManagerLog::getBetterState.
statep_t StateManagerLog::getState(metric_t p) {
  if(p<=0) throw std::out_of_range("State doesn't exist for p<=0");
  if(p>1) throw std::out_of_range("Probability should not be >1");

  metric_t q = ceil(log10(p)*m_nbPointsDec);
  
  // Index of the rounded metric
  stateid_t id = static_cast<stateid_t>(-1 * q);

  // Check if the state exists
  auto curState_iter = m_stateMap.find(id);
  if(curState_iter != m_stateMap.end()) {
    return curState_iter->second;
  }

  // Else: Create a new State
  metric_t Q = pow(10,q/m_nbPointsDec); // Quantized metric
  statep_t s(new State(id, Q));
  m_stateMap[id] = s;

  return s;
}

//Note: Same code as StateManagerLog::getState, but "ceil" is changed
//      to "floor".
statep_t StateManagerLog::getBetterState(metric_t p) {
  if(p<=0) throw std::out_of_range("State doesn't exist for p<=0");
  if(p>1) throw std::out_of_range("Probability should not be >1");

  metric_t q = floor(log10(p)*m_nbPointsDec);
  stateid_t id = static_cast<stateid_t>(-1 * q);
  auto curState_iter = m_stateMap.find(id);
  if(curState_iter != m_stateMap.end()) {
    return curState_iter->second;
  }
  metric_t Q = pow(10,q/m_nbPointsDec); // Quantized metric
  statep_t s(new State(id, Q));
  m_stateMap[id] = s;

  return s;
}


