//Author: Francois Leduc-Primeau

#ifndef ICost_hpp_
#define ICost_hpp_

#include <ostream>

class ICost {
public:
  virtual ~ICost() {}

  virtual ICost* getCopy() const =0;

  /**
   * Adds another cost to this cost.
   */
  virtual void operator+=(ICost const& cost) =0;

  /**
   * Increases the "penalty" cost, which is factored into the virtual cost.
   */
  virtual void addPenalty(double newPenalty) =0;

  /**
   * Whether this cost can be compared with c2, i.e. whether an
   * ordering is defined.
   */
  virtual bool isComparable(ICost const& c2) const =0;

  virtual bool operator==(ICost const& c2) const =0;

  virtual bool operator!=(ICost const& c2) const =0;

  /**
   * Whether this cost is smaller than c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   */
  virtual bool operator<(ICost const& c2) const =0;

  /**
   * Whether this cost is larger than c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   */
  virtual bool operator>(ICost const& c2) const =0;

  /**
   * Whether this cost is smaller or equal to c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   */
  virtual bool operator<=(ICost const& c2) const =0;

  /**
   * Whether this cost is larger or equal to c2.
   *@throws std::logic_error  if this cost and c2 are not comparable
   */
  virtual bool operator>=(ICost const& c2) const =0;

  /**
   * The virtual one-dimensional representation of the cost, to be
   * used by the path optimization. This might or might not correspond
   * to the "real" cost, and the precise meaning can vary depending on
   * the implementing class.
   */
  virtual double oneDRepresentation() const =0;

  /**
   * Returns the (real) energy associated with this Cost, if such a
   * parameter is defined. Returns a negative value if no energy
   * parameter is defined.
   */
  virtual double energy() const =0;
  
  /**
   * Returns the (real) latency associated with this Cost, if such a
   * parameter is defined. Returns a negative value if no latency
   * parameter is defined.
   */
  virtual unsigned int latency() const =0;

  virtual std::ostream& toStream(std::ostream& stream) const =0;

protected:
  ICost() {}

};

std::ostream& operator<<(std::ostream& stream, const ICost& c);

#endif
