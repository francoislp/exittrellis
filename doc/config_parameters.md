This document describes the configuration options.

## Key-Value Options

`config=<filepath>` The path to a file that contains configuration parameters.

`btwc_root=<filepath>` Path to the root directory of the "btwc_charac" project.

`ExitGen_files={<list of file paths>}` A list of file paths to ExitGen configuration files, each describing an EXIT curve that will be made available to the optimization engine. The file paths must be given with respect to `btwc_root`.

`po=<val>` *(float)* The error rate at the output of the channel.

`pres=<val>` *(float)* The target residual error. The actual target will be the quantized state closest to p_res that is better or equal to p_res.  

`quant=<val>` *(positive int)* Number of probability states per decade (using a logarithmic quantization)

`cost_type=<identifier>` Definition of cost to be used to rate paths in the trellis graph. Must be one of "energy", "energy-latency", or "edp" (without the quotes). "Energy" rates paths according to the energy cost only, "energy-latency" rates paths according to energy but discards those that exceed the specified maxLatency, and "edp" uses the product of energy and latency.

`maxLatency=<val>` *(positive int)* Latency constraint in picoseconds. For any `cost_type` that defines a latency, all solutions that take more the specified latency to reach the target residual error rate are discarded. If maxLatency is omitted or is 0, no constraint is applied.

`alg_change_penalty=<val>` *(positive float)* Penalty to be added to the energy cost of the path when the algorithm is changed from one iteration to the next. This penalty is only used for ranking paths and does not affect the energy result.

## Behavior Options

`--printEXITdata` Print interpolated EXIT data to standard output and exit. The quantization of the interpolation is controlled by `interpolTestStep`. The range covered goes from `po` to `interpolTestEnd`.

`--printSequence` Print to standard output the sequence of decoding rules associated with the optimal path.

`--printcmd`  Print the command line used to invoke the program to std out.
